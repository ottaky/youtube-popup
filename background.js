let our_left = 1200,
    our_top  = 230

chrome.contextMenus.onClicked.addListener(info => {

  let original_url = new URL(info.linkUrl),
      new_url

  if (   original_url.hostname.match(/google/)
      && original_url.searchParams.has('q')
      && original_url.searchParams.get('q').match(/youtu/)) {

    original_url = new URL(original_url.searchParams.get('q'))
  }

  if (original_url.hostname.match(/youtu/)) {

    new_url = new URL(  'https://www.youtube.com/embed/'
                      + (  original_url.searchParams.has('v')
                         ? original_url.searchParams.get('v')
                         : info.linkUrl.match(/(\/v\/|youtu\.be\/|v%3D)([A-Za-z0-9_-]+)/)[2]))

    new_url.searchParams.set('version',  '3')
    new_url.searchParams.set('rel',      '1')
    new_url.searchParams.set('autoplay', '1')

    if (original_url.searchParams.has('t')) {

      const t = original_url.searchParams.get('t')
      let start = 0

      if (t.match(/[m|s]/)) {

        if (t.match(/m/)) start += Math.floor(t.match(/([0-9]+)m/)[1]) * 60
        if (t.match(/s/)) start += Math.floor(t.match(/([0-9]+)s/)[1])
      }
      else {
        start = t
      }

      new_url.searchParams.set('start', start)
    }

    if (   original_url.searchParams.has('list')
        && original_url.searchParams.get('list').match(/^PL/)) {

      new_url.searchParams.set('listType', 'playlist')
      new_url.searchParams.set('list',     original_url.searchParams.get('list'))
    }
  }
  else if (original_url.hostname.match(/amazon/)) {

    const asin = original_url.searchParams.has('asin')
      ? original_url.searchParams.get('asin')
      : info.linkUrl.match(/(video\/detail|\/dp)\/([A-Z0-9]+)/)[2]

    new_url = new URL('https://www.amazon.co.uk/gp/video/detail/' + asin)

    new_url.searchParams.set('autoplay', '1')

    new_url.searchParams.set('t',
        original_url.searchParams.has('t')
      ? original_url.searchParams.get('t')
      : '0'
    )
  }
  else {
    new_url = new URL(info.linkUrl)
  }

  chrome.windows.create({
    url:    new_url.href,
    type:   chrome.windows.WindowType.POPUP,
    width:  640,
    height: 360,
    left:   our_left,
    top:    our_top
  },
  function(window) {
    if (original_url.hostname.match(/amazon/)) {
      // "autoplay" amazon videos - don't ask!
      chrome.tabs.executeScript(
        window.tabs[0].id, {
          code: `setTimeout(function(){document.getElementsByTagName('VIDEO')[0].play()},1000)`
        }
      )
    }
  })

  our_left = our_left - 20
  our_top  = our_top  + 20
})

chrome.runtime.onInstalled.addListener(() => {
  chrome.contextMenus.create({
    id:       'menu',
    title:    'YouTube Popup',
    contexts: ['link']
  })
})

