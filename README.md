Chrome extension to popup videos in a chromeless window.

Adds a YouTube Popup option in the right click menu for links.

Also works with Amazon videos.

Works with some other stuff. BBC iPlayer .. kind of.

One day I'll get around to making it work with **all** of the different YouTube link formats.

One day.

-=-=-

If you find that the popup fails to load / start the video correctly in chrome, you may
want to disable chrome://flags/#enable-quic 
